﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DataFinderInDataTable
{

    class Program
    {
        static void Main(string[] args)
        {
            DataTable dt = new DataTable();
            
            dt.Columns.Add("SSN", typeof(string));
            dt.Columns.Add("NAME", typeof(string));
            dt.Columns.Add("ADDR", typeof(string));
            dt.Columns.Add("AGE", typeof(int));

            // Showing how to set Primary Key(s) in a Data table (Although it's not compulsory to have one)
            DataColumn[] keys = new DataColumn[1];
            keys[0] = dt.Columns[0];
            dt.PrimaryKey = keys;

            dt.Rows.Add("203456876", "John", "12 Main Street, Newyork, NY", 15);
            dt.Rows.Add("203456877", "SAM", "13 Main Ct, Newyork, NY", 25);
            dt.Rows.Add("203456878", "Elan", "14 Main Street, Newyork, NY", 35);
            dt.Rows.Add("203456879", "Smith", "12 Main Street, Newyork, NY", 45);
            dt.Rows.Add("203456880", "SAM", "345 Main Ave, Dayton, OH", 55);
            dt.Rows.Add("203456881", "Sue", "32 Cranbrook Rd, Newyork, NY", 65);
            dt.Rows.Add("203456882", "Winston", "1208 Alex St, Newyork, NY", 65);
            dt.Rows.Add("203456883", "Mac", "126 Province Ave, Baltimore, NY", 85);
            dt.Rows.Add("203456884", "SAM", "126 Province Ave, Baltimore, NY", 95);

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine(" Retrieving all the person except having name 'SAM'\n");
            foreach (DataRow o in dt.Select("NAME <> 'SAM'"))
            {
                Console.WriteLine("\t" + o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine(" Retrieving Top 2 aged persons from the list who are older than 60 years\n");
            foreach (DataRow o in dt.Select("AGE > 60").Take(4))
            {
                Console.WriteLine("\t" + o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Checking whether any person is teen-ager or not...");
            if(dt.Select("AGE >= 13 AND AGE <= 19").Any())
            {
                Console.WriteLine("\t Yes, we have some teen-agers in the list");
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\nSorting data table in Descening order by AGE ");
             foreach (DataRow o in dt.Select("","AGE DESC"))
            {
                Console.WriteLine("\t" + o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
            }

             Console.WriteLine("\n-----------------------------------------------------------------------------");
             Console.WriteLine("\nSorting data table in Aescening order by NAME ");
             foreach (DataRow o in dt.Select("", "NAME ASC"))
             {
                 Console.WriteLine("\t" + o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
             }
            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Getting Average of all the person's age...");
            double avgAge = dt.Select("").Average(e => (int)e.ItemArray[3]);
            Console.WriteLine(" The average of all the person's age is: " + avgAge);

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Checking whether a person having name 'SAM' exists or not...");
            if(dt.Select().Any(e => e.ItemArray[1].ToString() == "SAM"))
            {
                Console.WriteLine("\tYes, A person having name  'SAM' exists in our list");
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Getting the name of the most aged person in the list ...");
            DataRow mostAgedPerson = dt.Select("", "AGE ASC").Last();
            Console.WriteLine("\t"+mostAgedPerson["SSN"] + "\t" + mostAgedPerson["NAME"] + "\t" + mostAgedPerson["ADDR"] + "\t" + mostAgedPerson["AGE"]);

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Getting Sum of all the person's age...");
            int sumOfAges =dt.Select().Sum(e=> (int) e.ItemArray[3]);
            Console.WriteLine("\t The sum of all the persons's age = " + sumOfAges);

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Skipping every person whose age is less than 60 years...");
            foreach (DataRow o in dt.Select().SkipWhile(e=> (int)e.ItemArray[3] <60))
            {
                Console.WriteLine("\t"+o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine(" Displaying the persons until we find a person with name starts with other than 'S'");
            foreach (DataRow o in dt.Select().Where(e=> e.ItemArray[1].ToString().StartsWith("S")))
            {
                Console.WriteLine("\t" + o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Checking all the persons have SSN or not ...");
            if(dt.Select().All(e => e.ItemArray[0] != null))
            {
                Console.WriteLine("\t No person is found without SSN");
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.WriteLine("\n Finding the person whose SSN = 203456876 in the list");
            foreach (DataRow o in dt.Select("SSN = '203456876'"))
            {
                Console.WriteLine("\t" + o["SSN"] + "\t" + o["NAME"] + "\t" + o["ADDR"] + "\t" + o["AGE"]);
            }
            Console.WriteLine("\n-----------------------------------------------------------------------------");
            Console.Read();
        }
    }
}
